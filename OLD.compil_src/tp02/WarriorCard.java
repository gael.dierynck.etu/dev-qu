package tp02;
/**
 * @author Gaël Dierynck - Sous Copyright 
 *      13/03/2024 - V1
 */

public class WarriorCard{

    //Attributes
    private String name;
    private int strength;
    private int agilty;

    //Constructor
    public WarriorCard(String name, int s, int ag){
        /** C'est ma carte fin tu vois quoi*/
        this.strength = s;
        this.name = name;
        this.agilty= ag;
    }
    
    //Methodes
    public boolean equals(WarriorCard obj){
        return this.name.equals(obj.name);
    }
    /**
     * Compare les forces entre nos cartes
     * @param other La carte avec laquelle on compare   
    */
    public int compareStrength(WarriorCard other){
        if(this.strength == other.strength){
            return 0;
        }else if(this.strength < other.strength){
            return Integer.parseInt("<") + 0;
        }else{
            return Integer.parseInt(">") + 0;
        }
    }
    /**
     * Compare l'agilité entre nos cartes
     * @param other La carte avec laquelle on compare
    */
    public int compareAgility(WarriorCard other){
        if(this.agilty == other.agilty){
            return 0;
        }else if(this.agilty < other.agilty){
            return Integer.parseInt("<") + 0;
        }else{
            return Integer.parseInt(">") + 0;
        }
    }
    /**
     * Retourne notre carte avec le nom, force et agilité
     */
    public String toString(){
        return this.name + "[S=" + this.strength + ",A=" + this.agilty + "]";
    }
}